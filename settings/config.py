import os
from pathlib import Path
from typing import Optional

import dotenv

from backend_tools.backend_tools.settings import BaseBackendSettings
from backend_tools.backend_tools import tortoise_orm

BASE_DIR = Path(__file__).resolve().parent.parent


class Settings(BaseBackendSettings):
    DEBUG: bool = False
    DEV: bool = True

    APP_URI_PREFIX: Optional[str]

    PG_HOST: str
    PG_PORT: int
    PG_USER: str
    PG_PASSWORD: str
    PG_DATABASE: str

    class Config:
        os.environ['FASTAPI_TITLE'] = 'Planificados Service API'
        env_file = Path(BASE_DIR, 'settings', 'env')
        dotenv.load_dotenv(env_file)

    def get_app_uri_prefix(self) -> str:
        if self.APP_URI_PREFIX is None:
            return ''
        return settings.APP_URI_PREFIX.strip('/')

    def create_tortoise_config(self) -> tortoise_orm.TortoiseOrmConfig:
        return tortoise_orm.create_default_config(
            credentials=tortoise_orm.DbCredentials(
                host=self.PG_HOST,
                port=self.PG_PORT,
                database=self.PG_DATABASE,
                user=self.PG_USER,
                password=self.PG_PASSWORD
            ),
            model_files=['app.database']
        )


settings = Settings()
TORTOISE_ORM = settings.create_tortoise_config().to_init_params()
