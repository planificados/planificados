FROM python:3.9.7 as stage-build

ARG DIR=/usr/src/app
ARG VENV=$DIR/.venv

ENV  PATH="$VENV/bin:$PATH" \
     POETRY_VERSION=1.1.6

RUN mkdir -p $DIR
WORKDIR $DIR

RUN apt-get update && \
    apt-get install -y locales git && \
    sed -i -e 's/# ru_RU.UTF-8 UTF-8/ru_RU.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales

ENV TZ=Europe/Moscow \
    LANG=ru_RU.UTF-8 \
    LANGUAGE=ru_RU \
    LC_ALL=ru_RU.UTF-8
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone

COPY pyproject.toml poetry.lock ./

RUN pip install -U pip && \
    pip install "poetry==$POETRY_VERSION" && \
    poetry run pip install -U pip

COPY . .


FROM stage-build as stage-ci

RUN poetry export --dev --no-interaction --no-ansi --without-hashes -f requirements.txt -o requirements.txt && \
    pip install --prefix=$VENV --force-reinstall -r requirements.txt


FROM stage-build as stage-prod

RUN poetry export --no-interaction --no-ansi --without-hashes -f requirements.txt -o requirements.txt && \
    pip install --prefix=$VENV --force-reinstall -r requirements.txt

#CMD sh -c "aerich upgrade && uvicorn run:app --host 127.0.0.1 --port 5000"
CMD sh -c "uvicorn run:app --host 0.0.0.0 --port 5000"