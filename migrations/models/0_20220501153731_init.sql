-- upgrade --
CREATE TABLE IF NOT EXISTS "journal" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "action" TEXT NOT NULL,
    "data" JSONB,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX IF NOT EXISTS "idx_journal_created_670d8c" ON "journal" ("created_at");
CREATE TABLE IF NOT EXISTS "users" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "first_name" VARCHAR(100),
    "last_name" VARCHAR(100),
    "email" VARCHAR(255),
    "phone_number" VARCHAR(20),
    "role" VARCHAR(255) NOT NULL  DEFAULT 'client',
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX IF NOT EXISTS "idx_users_email_133a6f" ON "users" ("email");
COMMENT ON COLUMN "users"."role" IS 'ADMIN: admin\nCLIENT: client\nWORKER: worker\nMANAGER: manager';
CREATE TABLE IF NOT EXISTS "messenger_users" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "type" VARCHAR(128) NOT NULL,
    "messenger_id" VARCHAR(50) NOT NULL,
    "user_name" VARCHAR(100) NOT NULL,
    "data" JSONB,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "user_id" UUID NOT NULL REFERENCES "users" ("id") ON DELETE CASCADE
);
COMMENT ON COLUMN "messenger_users"."type" IS 'TELEGRAM: telegram\nVIBER: viber';
CREATE TABLE IF NOT EXISTS "services" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "name" VARCHAR(200) NOT NULL,
    "description" TEXT NOT NULL,
    "execution_time_minutes" BIGINT NOT NULL,
    "is_deleted" BOOL NOT NULL  DEFAULT False,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "worker_id" UUID NOT NULL REFERENCES "users" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "timetable" (
    "id" UUID NOT NULL  PRIMARY KEY,
    "start_at" TIMESTAMPTZ,
    "end_at" TIMESTAMPTZ,
    "is_deleted" BOOL NOT NULL  DEFAULT False,
    "created_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "client_id" UUID NOT NULL REFERENCES "users" ("id") ON DELETE CASCADE,
    "service_id" UUID NOT NULL REFERENCES "services" ("id") ON DELETE CASCADE,
    "worker_id" UUID NOT NULL REFERENCES "users" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(100) NOT NULL,
    "content" JSONB NOT NULL
);
