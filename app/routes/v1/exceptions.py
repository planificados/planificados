from typing import Any, Dict, Optional

from fastapi import HTTPException
from pydantic import BaseModel

from planificados_tools.planificados_tools.api.core import models


class ErrorInfo(BaseModel):
    code: int
    message: str

    @classmethod
    def from_response_status(cls, response_status: models.ResponseStatus):
        return cls(code=response_status.value, message=response_status.name)


class PlanificadosException(HTTPException):
    def __init__(
            self,
            status_code: int,
            response_status: models.ResponseStatus,
            headers: Optional[Dict[str, Any]] = None
    ):
        super().__init__(
            status_code=status_code,
            detail=ErrorInfo.from_response_status(response_status=response_status).dict(),
            headers=headers
        )
