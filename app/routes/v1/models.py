from typing import Any, Dict, Iterable, Union, Optional

from pydantic import root_validator, validator
from tortoise.contrib.pydantic import pydantic_model_creator

from app import database
from planificados_tools.planificados_tools.api.core import models


def _normalize_name(name: Optional[str]) -> Optional[str]:
    if name is None:
        return name
    return name.lower().strip()


def _check_completion_one_value(values: Iterable, message: str):
    assert any([value is not None for value in values]), message


def _check_messenger_ids(values: Dict[str, Any]):
    ids = (values.get('telegram_id'), values.get('viber_id'))
    _check_completion_one_value(values=ids, message='One of the fields (`telegram_id` or `viber_id`) must be filled')


class MessageResponseVld(models.MessageResponse):
    pass


class MessengerIdsVld(models.MessengerIds):
    @root_validator(pre=True)
    def check_filling(cls, values: Dict[str, Any]) -> Dict[str, Any]:
        _check_messenger_ids(values=values)
        return values


_DatabaseBasedUserModel = pydantic_model_creator(database.User)


class UserVld(models.User):
    _normalize_name = validator('email', allow_reuse=True)(_normalize_name)

    @root_validator(pre=True)
    def check_filling(cls, values: Dict[str, Any]):
        _check_messenger_ids(values=values)
        # names = (value for field, value in values.items() if field.endswith('name'))
        # _check_completion_one_value(values=names, message='One of the name fields must be specified')
        return values

    @classmethod
    def from_orm(cls, obj: database.User) -> 'UserVld':
        model_obj = cls(
            first_name=obj.first_name,
            last_name=obj.last_name,
            email=obj.email,
            phone_number=obj.phone_number
        )
        if obj.telegram is not None:
            model_obj.telegram_id = obj.telegram.messanger_id
            model_obj.telegram_name = obj.telegram.user_name
        if obj.viber is not None:
            model_obj.viber_id = obj.viber.messanger_id
            model_obj.viber_name = obj.viber.user_name
        return model_obj

    def to_orm(self) -> database.User:
        return database.User(
            first_name=self.first_name,
            last_name=self.last_name,
            email=self.email,
            phone_number=self.phone_number,
            telegram_id=self.telegram_id,
            telegram_name=self.telegram_name,
            viber_id=self.viber_id,
            viber_name=self.viber_name
        )

    async def to_orm_dict(self) -> dict:
        database_obj = self.to_orm()
        pydantic_obj = await _DatabaseBasedUserModel.from_tortoise_orm(database_obj)
        return pydantic_obj.dict()


class UserResponseVld(models.UserResponse):
    @classmethod
    def from_orm(cls, obj: database.User) -> 'UserResponseVld':
        model_obj = cls(
            first_name=obj.first_name,
            last_name=obj.last_name,
            email=obj.email,
            phone_number=obj.phone_number,
            id=obj.id,
            is_admin=obj.is_admin,
            role=obj.role
        )
        if obj.telegram is not None:
            model_obj.telegram_id = obj.telegram.messanger_id
            model_obj.telegram_name = obj.telegram.user_name
        if obj.viber is not None:
            model_obj.viber_id = obj.viber.messanger_id
            model_obj.viber_name = obj.viber.user_name
        return model_obj


class ServiceVld(models.Service):
    _normalize_name = validator('name', allow_reuse=True)(_normalize_name)


class GetServiceResponseVld(models.GetServiceResponse):
    # @classmethod
    # def from_orm(cls, obj: database.Service):
    #     obj.execution_time_minutes = obj.execution_time_minutes.seconds // 60
    #     return super().from_orm(obj=obj)
    pass


class CreateServiceVld(models.CreateService):
    _normalize_name = validator('name', allow_reuse=True)(_normalize_name)


class UpdateServiceVld(CreateServiceVld):
    pass


class TimetableResponseVld(models.TimetableResponse):
    pass


class AuthResponseVld(models.AuthResponse):
    pass


class GetTokenVld(models.GetToken):
    @root_validator(pre=True)
    def check_filling(cls, values: Dict[str, Any]) -> Dict[str, Any]:
        _check_messenger_ids(values=values)
        return values
