from uuid import UUID

from backend_tools.backend_tools.fastapi.routers import create_router

router = create_router(prefix='/timetable', tags=['Manage timetable'])


# @router.get('', response_model=models.ItemsVld[models.TimetableResponseVld])
# async def get_slots():
#     pass


@router.post('/create')
async def create_slots():
    pass


@router.put('/{slotId}')
async def update_slot(slot_id: UUID):
    pass


@router.delete('/{slotId}')
async def delete_slot(slot_id: UUID):
    pass


@router.get('/free')
async def get_free_slots():
    pass


@router.put('/take')
async def take_slots():
    pass


@router.put('/release')
async def release_slots():
    pass
