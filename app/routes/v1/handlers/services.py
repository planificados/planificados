from datetime import timedelta
from uuid import UUID

from fastapi import Depends, status
from tortoise.transactions import in_transaction

from app import database
from app.routes.v1 import models as models_vld
from app.routes.v1.exceptions import PlanificadosException
from backend_tools.backend_tools.fastapi.routers import create_router
from planificados_tools.planificados_tools.api.core import models
from utils.security import get_user_by_token

router = create_router(prefix='/services', tags=['Manage services'])

forbidden_exception = PlanificadosException(
    status_code=status.HTTP_403_FORBIDDEN,
    response_status=models.ResponseStatus.ADMIN_AND_WORKER_ACCESS_ONLY
)


async def get_service_for_edit(service_id: UUID, current_user: database.User) -> database.Service:
    if not (current_user.is_admin or current_user.role == models.UserRole.WORKER):
        raise forbidden_exception

    service_obj = await database.Service.get_or_none(id=service_id).prefetch_related('worker')
    if not service_obj:
        raise PlanificadosException(
            status_code=status.HTTP_400_BAD_REQUEST,
            response_status=models.ResponseStatus.NO_SERVICES_FOUND
        )
    if current_user.role == models.UserRole.WORKER and service_obj.worker.id != current_user.id:
        raise forbidden_exception
    return service_obj


def create_service_obj(worker: database.User, payload: models_vld.CreateServiceVld) -> database.Service:
    data = payload.dict()
    data.pop('execution_time_minutes')
    return database.Service(
        worker=worker,
        execution_time_minutes=timedelta(minutes=payload.execution_time_minutes),
        **data
    )


# @router.get('', response_model=models.ItemsVld[models.GetServiceResponseVld])
# async def get_services(current_user: database.User = Depends(get_user_by_token)):
#     return models.ItemsVld(items=await database.Service.filter(is_deleted=False))


# @router.post('/create', response_model=models.ItemsVld[models.GetServiceResponseVld])
# async def create_services(
#         payload: models.ItemsVld[models.CreateServiceVld],
#         worker_id: UUID = None,
#         current_user: database.User = Depends(get_user_by_token)
# ):
#     if current_user.is_admin:
#         if worker_id is None:
#             raise PlanificadosException(
#                 status_code=status.HTTP_400_BAD_REQUEST,
#                 response_status=models.ResponseStatus.MISSING_WORKER_ID
#             )
#         worker = await get_user_by_id(user_id=worker_id)
#     elif current_user.type == models.UserRole.WORKER:
#         worker = current_user
#     else:
#         raise forbidden_exception
#
#     services = [create_service_obj(worker=worker, payload=item) for item in payload.items]
#     async with in_transaction():
#         # TypeError: object of type 'asyncpg.pgproto.pgproto.WriteBuffer' has no len()
#         # await Services.bulk_create(services)
#         for service in services:
#             await service.save()
#         await database.Journal.create(action=f'User={current_user.id} created services={services}')
#     return models.ItemsVld(items=await database.Service.filter(is_deleted=False))


@router.put('/{serviceId}', response_model=models_vld.GetServiceResponseVld)
async def update_service(
        service_id: UUID,
        payload: models_vld.UpdateServiceVld,
        current_user: database.User = Depends(get_user_by_token)
):
    service_obj = await get_service_for_edit(service_id=service_id, current_user=current_user)
    service_data = payload.dict(exclude_none=True)
    async with in_transaction():
        await service_obj.update_from_dict(service_data)
        await service_obj.save()
        await database.Journal.create(
            action=f'User={current_user.id} updated service={service_obj.id}, data={service_data}'
        )
    return service_obj


@router.delete('/{serviceId}', response_model=models_vld.MessageResponseVld)
async def delete_service(service_id: UUID, current_user: database.User = Depends(get_user_by_token)):
    service_obj = await get_service_for_edit(service_id=service_id, current_user=current_user)
    service_obj.is_deleted = True
    await service_obj.save()
    return models_vld.MessageResponseVld(message='deleted')
