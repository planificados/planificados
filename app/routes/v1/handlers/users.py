from uuid import UUID

from fastapi import Depends, status, Path
from tortoise import exceptions as tortoise_exceptions
from tortoise.transactions import in_transaction

from app import database
from app.routes.v1 import models as models_vld
from app.routes.v1.exceptions import PlanificadosException
from backend_tools.backend_tools.fastapi.routers import create_router
from planificados_tools.planificados_tools.api.core import models
from settings.config import settings
from utils.security import get_bearer_token, get_user_by_token

router = create_router(prefix='/users', tags=['Manage users'])


# async def get_user_by_messenger(telegram_id: str, viber_id: str, raise_exception=True) -> database.User:
#     user_obj = await database.User.get_or_none(Q(telegram_id=telegram_id) | Q(viber_id=viber_id))
#     if not user_obj and raise_exception:
#         raise PlanificadosException(
#             status_code=status.HTTP_400_BAD_REQUEST,
#             response_status=models.ResponseStatus.NO_USER_FOUND
#         )
#     return user_obj


@router.post('/token/auth', response_model=models.AuthResponse)
async def get_user_token(payload: models_vld.GetTokenVld):
    try:
        user_obj = await database.User.get_by_messengers(telegram_id=payload.telegram_id, viber_id=payload.viber_id)
    except tortoise_exceptions.DoesNotExist:
        raise PlanificadosException(
            status_code=status.HTTP_400_BAD_REQUEST,
            response_status=models.ResponseStatus.NO_USER_FOUND
        )
    await database.Journal.create(action=f'User={user_obj.id} logged in')
    return get_bearer_token(user_id=user_obj.id)


@router.get('/me', response_model=models.UserResponse)
async def get_me(current_user: database.User = Depends(get_user_by_token)):
    return models_vld.UserVld.from_orm(obj=current_user)


@router.get('/{userId}', response_model=models.UserResponse)
async def get_user(user_id: UUID = Path(..., alias='userId'), current_user: database.User = Depends(get_user_by_token)):
    user_obj = await get_user_by_id(user_id=user_id)
    return models_vld.UserResponseVld.from_orm(obj=user_obj)


@router.post('/create-admin', response_model=models.UserResponse, include_in_schema=False)
async def create_admin():
    async with in_transaction():
        user_obj, _ = await database.User.get_or_create(
            email='admin@admin.ru',
            role=models.UserRole.ADMIN,
            defaults={
                'first_name': 'admin',
                'last_name': 'admin',
            }
        )
        await database.Journal.create(action=f'Created admin={user_obj.id}')
    return user_obj


@router.post('/create', response_model=models.UserResponse)
async def create_user(payload: models_vld.UserVld):
    try:
        await database.User.get_by_messengers(telegram_id=payload.telegram_id, viber_id=payload.viber_id)
    except tortoise_exceptions.DoesNotExist:
        pass
    else:
        raise PlanificadosException(
            status_code=status.HTTP_400_BAD_REQUEST,
            response_status=models.ResponseStatus.USER_ALREADY_EXISTS
        )

    user_obj = payload.to_orm()
    async with in_transaction():
        await user_obj.save()
        await database.Journal.create(action=f'Created user={user_obj.id}')
    return user_obj


@router.put('/', response_model=models.UserResponse)
async def update_user(payload: models_vld.UserVld, current_user: database.User = Depends(get_user_by_token)):
    user_data = await payload.to_orm_dict()
    async with in_transaction():
        await current_user.update_from_dict(data=user_data)
        # await current_user.save()
        await database.Journal.create(action=f'User={current_user.id} updated data={user_data}')
    return current_user


# @router.put('/role/{userId}', response_model=models.UserResponse)
# async def turn_into_worker(user_id: UUID, current_admin: database.User = Depends(get_admin_by_token)):
#     user_obj = await get_user_by_id(user_id=user_id)
#     user_obj.type = models.UserRole.WORKER
#     async with in_transaction():
#         await user_obj.save()
#         await database.Journal.create(action=f'Admin={current_admin.id} turned client={user_obj.id} into worker')
#     return user_obj


# @router.get('/workers', response_model=models.ItemsVld[models.UserResponseVld])
# async def get_workers(current_user: database.User = Depends(get_user_by_token)):
#     return models.ItemsVld(items=await database.User.filter(type=models.UserRole.WORKER))


async def get_user_by_id(user_id: UUID, raise_exception=True) -> database.User:
    user_obj = await database.User.get_or_none(id=user_id)
    if not user_obj and raise_exception:
        raise PlanificadosException(
            status_code=status.HTTP_400_BAD_REQUEST,
            response_status=models.ResponseStatus.NO_USER_FOUND
        )
    return user_obj
