from app.routes.v1.handlers import services, timetable, users
from backend_tools.backend_tools.fastapi.routers import add_version_prefix

VERSION = 'v1'


routers = [
    add_version_prefix(router=services.router, app_version_prefix=VERSION),
    add_version_prefix(router=timetable.router, app_version_prefix=VERSION),
    add_version_prefix(router=users.router, app_version_prefix=VERSION),
]
