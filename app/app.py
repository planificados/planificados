from app.routes.v1.app import routers
from app.routes.v1.handlers import users
from backend_tools.backend_tools.fastapi.routers import register_routers
from backend_tools.backend_tools.logger import init_logger
from backend_tools.backend_tools.sentry import init_sentry
from backend_tools.backend_tools.tortoise_orm import TortoiseOrm
from settings.config import settings, TORTOISE_ORM

init_logger(is_debug=settings.DEBUG)
init_sentry(config=settings.create_sentry_config())


db_orm = TortoiseOrm(config=TORTOISE_ORM, generate_schemas=False)
app = register_routers(
    app_uri_prefix=settings.get_app_uri_prefix(),
    routers=routers,
    on_startup=[db_orm.init, users.create_admin],
    on_shutdown=[db_orm.close]
)
