from datetime import datetime
from typing import Optional

from tortoise import fields, Model
from tortoise.expressions import Q
from tortoise.queryset import QuerySet
from tortoise.transactions import in_transaction

from planificados_tools.planificados_tools.api.core import models


class User(Model):
    id = fields.UUIDField(pk=True)
    first_name = fields.CharField(max_length=100, null=True)
    last_name = fields.CharField(max_length=100, null=True)
    email = fields.CharField(max_length=255, index=True, null=True)
    phone_number = fields.CharField(max_length=20, null=True)
    role = fields.CharEnumField(enum_type=models.UserRole, max_length=255, default=models.UserRole.CLIENT)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)

    class Meta:
        table = 'users'


class MessengerUser(Model):
    id = fields.UUIDField(pk=True)
    user = fields.ForeignKeyField('models.User', related_name='messenger_user', on_delete=fields.CASCADE)
    type = fields.CharEnumField(enum_type=models.MessengerType, max_length=128)
    messenger_id = fields.CharField(max_length=50)
    user_name = fields.CharField(max_length=100)
    data = fields.JSONField(null=True)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)

    class Meta:
        table = 'messenger_users'

    @classmethod
    def filter_by_messenger(
            cls,
            messenger_type: models.MessengerType,
            messenger_id: str,
            query: Optional[QuerySet]
    ) -> QuerySet:
        if query is None:
            query = cls.filter()
        return query.filter(type=messenger_type, messenger_id=messenger_id)


class Service(Model):
    id = fields.UUIDField(pk=True)
    name = fields.CharField(max_length=200)
    description = fields.TextField()
    execution_time_minutes = fields.TimeDeltaField()
    is_deleted = fields.BooleanField(default=False)
    worker = fields.ForeignKeyField('models.User', related_name='service')
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)

    class Meta:
        table = 'services'

    async def mark_as_deleted(self):
        self.is_deleted = True
        async with in_transaction():
            await Timetable.filter(service=self.id, start_at__gte=datetime.utcnow()).update(is_deleted=True)
            await self.save()


class Timetable(Model):
    id = fields.UUIDField(pk=True)
    worker = fields.ForeignKeyField('models.User', related_name='timetable_worker')
    client = fields.ForeignKeyField('models.User', related_name='timetable_client')
    service = fields.ForeignKeyField('models.Service', related_name='timetable_service')
    start_at = fields.DatetimeField(null=True)
    end_at = fields.DatetimeField(null=True)
    is_deleted = fields.BooleanField(default=False)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)

    async def mark_as_deleted(self):
        self.is_deleted = True
        await self.save()

    class Meta:
        table = 'timetable'


class Journal(Model):
    id = fields.UUIDField(pk=True)
    action = fields.TextField()
    data = fields.JSONField(null=True)
    created_at = fields.DatetimeField(auto_now_add=True, index=True)
    updated_at = fields.DatetimeField(auto_now=True)

    class Meta:
        table = 'journal'
