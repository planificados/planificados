from datetime import datetime, timedelta
from typing import Optional
from uuid import UUID

import jwt
from fastapi import Depends, status
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from jwt import PyJWTError
from passlib.context import CryptContext

from app import database
from app.routes.v1 import models as models_vld
from app.routes.v1.exceptions import PlanificadosException
from planificados_tools.planificados_tools.api.core import models

SECRET_KEY = '6f63b88e8d3e77099f6f09d25e06b7a9563b93fa2556c81810f4caa6c94faa6c'
ALGORITHM = 'HS256'
ACCESS_TOKEN_EXPIRE_MINUTES = 60 * 24

pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')
bearer_scheme = HTTPBearer()


def create_access_token(data: dict, expires_delta: timedelta, secret_key: str = SECRET_KEY) -> str:
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta
    to_encode.update({'exp': int(expire.timestamp())})
    return jwt.encode(to_encode, secret_key, algorithm=ALGORITHM)


def get_bearer_token(user_id: UUID, expires_delta: Optional[timedelta] = None) -> models_vld.AuthResponseVld:
    if expires_delta is None:
        expires_delta = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)

    access_token = create_access_token(
        data={
            'user_id': str(user_id),
            'created_at': str(datetime.utcnow())
        },
        expires_delta=expires_delta
    )
    return models_vld.AuthResponseVld(access_token=access_token, token_type=models.TokenType.BEARER)


async def get_user_by_token(token: HTTPAuthorizationCredentials = Depends(bearer_scheme)) -> database.User:
    credentials_exception = PlanificadosException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        response_status=models.ResponseStatus.INVALID_CREDENTIALS,
    )
    try:
        payload = jwt.decode(token.credentials, SECRET_KEY, algorithms=[ALGORITHM])
    except PyJWTError:
        raise credentials_exception

    user_id: str = payload.get('user_id')
    if user_id is None:
        raise credentials_exception

    query = database.User.with_relations()
    user = await query.get_or_none(id=user_id)
    if not user:
        raise credentials_exception
    return user


async def get_admin_by_token(current_user: database.User = Depends(get_user_by_token)) -> database.User:
    if not current_user.is_admin:
        raise PlanificadosException(
            status_code=status.HTTP_403_FORBIDDEN,
            response_status=models.ResponseStatus.ADMIN_ACCESS_ONLY
        )
    return current_user


async def get_worker_by_token(current_user: database.User = Depends(get_user_by_token)) -> database.User:
    if current_user.role != models.UserRole.WORKER:
        raise PlanificadosException(
            status_code=status.HTTP_403_FORBIDDEN,
            response_status=models.ResponseStatus.WORKER_ACCESS_ONLY
        )
    return current_user
