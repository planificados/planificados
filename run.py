import uvicorn

from app.app import app
from backend_tools.backend_tools.logger import configs
from settings.config import settings

uvicorn.Config(
    app=app,
    proxy_headers=True,
    access_log=False,
    use_colors=False,
    log_config=configs.UVICORN_LOGGING_CONFIG,
)

if __name__ == '__main__':
    uvicorn.run(app=app, debug=settings.DEBUG, log_config=configs.UVICORN_LOGGING_CONFIG, port=8000)
